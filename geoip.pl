#!/usr/bin/perl
use strict;
use warnings;

# Check if the file name is provided
my $filename = shift @ARGV or die "Usage: $0 <filename>\n";

# Open the file for reading
open my $fh, '<', $filename or die "Could not open file '$filename': $!\n";

# Process each line in the file
while (my $ip = <$fh>) {
  chomp $ip;       # Remove newline character
  #  next if $ip =~ /^\s*$/;       # Skip empty lines

  my $output = `geoiplookup $ip`;

  if ($? == 0) {
		$output = substr($output, 22);
    print "$ip;$output";
  }
  # else {
  #   # Print an error message if geoiplookup fails
  #   print "Error looking up IP $ip: $!\n";
  # }
}
close $fh;
