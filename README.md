# sshbanbrute

A crude way to detect SSH brute force attacks source IPs, then instruct the firewall to block them (they will be unable to reach the SSH port).

Similar to SSHGuard and Fail2ban.

For Linux.

Made of bash scripts using systemd, iptables (ipset) and PostgreSQL.

Also uses 'last' in order to never ban an IP source used during a recent successful login.

# Security

All scripts are 'set -eu' and therefore automagically fail upon any error or reference to an unset variable.

Some scripts source our 'attackers_parms' file, sometimes in a root context, therefore for security-related reasons they MUST pertain to the user 'root' and the group 'root', and MUST NOT be writable by any other account.
The scripts check all this before doing anything potentially dangerous.

This combination of 'set -eu' and checks are sufficient as long as the scripts cannot be modified by a hostile party.

To enhance security you may 'chattr +i' 'attackers_parms' and all scripts.

# Installation

Edit the 'attackers_parms' file, establish configuration variables:
- opt_verbose : put any value in it in order for the script to become verbose
- IPSET_NAME : name of the ipset set we use
- CENTRAL_DIR : the litteral name (no '~' or similar) of a local directory where the scripts stay.  End it with a slash '/'.  It will store some data files: 'ssh_addresses_ok.txt' (those IP addresses were used to log into the system and will never be banned), "IPSET_NAME.ipset" our ipset set, "IPSET_NAME_login_name.txt" account names submitted during the attacks
- UNPRIV_USERNAME : a local system (Linux) account username used by some scripts.  His home must contain a 'tmp/' directory.
- DBNAME : name of the PostgreSQL database we use
- DEFAULT_PENALTY : ipset (ban) timeout (seconds), after this delay a newly added banned address will be unbanned.  Hint: if you use the optionnal [Proportional ban duration](#proportional-ban-duration) feature then a low value is more convienient as a source IP banned by mistake will quickly get unbanned.

Copy the files 'attackers_parms', 'attackers_upd', 'attackers_penalty.sql' and 'attackers_penalty_enforce' in the directory named by CENTRAL_DIR

Ensure that all those files belong to 'root:root' and can only be modified by 'root'.

As root create our ipset set ([intro to ipset](https://wiki.archlinux.org/title/Ipset#Blocking_a_list_of_IP_addresses)):
``` shell
ipset create ${IPSET_NAME} hash:ip timeout 43200 forceadd
```

Do what must be done in order for the following commands to be executed (as root) during each boot, just before up'ing network interfaces. Replace 'SSHPORT' by the TCP port number listened to by your sshd (usually 22):
``` shell
ipset restore -file ${CENTRAL_DIR}${IPSET_NAME}.ipset
iptables -I INPUT -p tcp --dport SSHPORT -m set --match-set attacker src -j DROP
```

Using ''psql'': connect to the database $DBNAME then create the table which will store events (each tuple provides a source IP detected and the detection timestamp):
``` postgresql
CREATE TABLE attacker_ip (
    add inet NOT NULL,
    detected timestamp with time zone DEFAULT now()
);
CREATE INDEX attacker_ip_add_idx ON public.attacker_ip USING btree (add);
```

Test, as root, by invoking the 'attackers_upd' script.

Add a crontab (or equivalent) mission definition (replace ''CENTRAL_DIR'' by the adequate directory name):
```
11 * * * *  sudo CENTRAL_DIR/attackers_upd
```

# Usage

## Shell

Our ipset (addresses currently banned) list:
``` shell
sudo ipset list ${IPSET_NAME}
```

## PostgreSQL

Recent events:
``` postgresql
select add from attacker_ip where now()-detected <= '1 day' order by add desc;
```

Addresses detected more than once and their respectives amount of events:
``` postgresql
select add,count(*) as detections from attacker_ip group by add having count(*)>1 order by detections desc;
```

Distinct addresses and their detection timestamps intervals:
``` postgresql
-- mintimeframe: any source IP attacking during less than this delay will not be reported
\set mintimeframe 6hours
with ips as (select distinct add,(select min(detected) from attacker_ip t where t.add=p.add) as oldest, (select max(detected) from attacker_ip t where t.add=p.add) as newest from attacker_ip p) select add, newest-oldest as timeframe,oldest,newest from ips group by add,newest-oldest,oldest,newest having newest-oldest>:'mintimeframe' order by timeframe desc;
```

To semi-purge the table (DANGER!  For each source IP address it only leaves the most ancient event in the table):
``` postgresql
select distinct add,(select min(detected) from attacker_ip t where t.add=p.add) as detected into temp tmp1 from attacker_ip p order by add;
truncate attacker_ip;
insert into attacker_ip select * from tmp1;
```

## Proportional ban duration

This is optional and will set, for each address, a ban duration proportional to the amount of associated events.
By default each address is only banned for DEFAULT_PENALTY seconds.

### Installation

#### Create a table

``` postgresql
CREATE TABLE attacker_penalty (
    add inet NOT NULL PRIMARY KEY,
    penalty integer not NULL,
    enforced timestamp with time zone DEFAULT now()
);
```

#### Set parameters

Edit the 'attackers_penalty.sql' file and set its parameters:
- attack_penalty_max_age : an address will be neglected if its last event is older
- attack_penalty_base : minimal ban duration (seconds) for any address with at least 2 events
- attack_penalty_exponent : exponent applied to the amount of events to obtain 'intensity'
- attack_penalty_unit : duration (seconds) of the penalty per 'intensity' unit
- attack_penalty_max : max total penalty (seconds)

### Usage

Periodically (once per day?):
1. as the user ${UNPRIV_USERNAME}: execute the 'attackers_penalty.sql' SQL script, which will produce a shell script (named 'attackers_penalty_set.sh') establishing new penalties
2. if 'attackers_penalty_set.sh' isn't empty then (as root) execute it

The 'attackers_penalty_enforce' shell script does all this, here is a crontab mission definition (replace ''CENTRAL_DIR'' by the adequate directory name):
```
15 */2 * * *  CENTRAL_DIR/attackers_penalty_enforce
```

Recent activity (reporting):
``` postgresql
\set lapse 1day
\echo News recidivists:
select add as address,penalty from attacker_penalty where now()-enforced<= :'lapse' order by add desc;
\echo New events (no penalty yet):
select add as address from attacker_ip i left join attacker_penalty a using (add) where a.add is null and now()-detected<= :'lapse' order by add desc;
```

# Published blacklist

1. [Author's](http://makarevitch.com/blacklist/)

# TODO

## Directories
Add a DATA_DIR parameter and abstain from storing anything into CENTRAL_DIR, in order for it to be read-only (protecting the scripts).

## No useless priv
Let all scripts run as a nonpriv user, use as few sudo invocations as possible(?)

## SQLite
Add an 'opt_rdbms' parameter (1 for psql, 2 for sqlite).
Implementation: maybe thanks to "GNU sql"?
``` sqlite
.import --csv --schema attacker_ip
```

## Blacklists
https://metacpan.org/pod/Net::Abuse::Utils::Spamhaus

### IP-BlackHole
https://github.com/BlackHoleMonster/IP-BlackHole

### AbuseIPDB
Report to https://www.abuseipdb.com/ , see https://metacpan.org/pod/WebService::AbuseIPDB

To prepare a [bulk report](https://www.abuseipdb.com/bulk-report):
``` postgresql
\set lapse 1 hour
select add as "IP",'"18,22"' as "Categories",enforced as "ReportDate",'"SSH brute-force attack"' as "Comment" from attacker_penalty where now()-enforced<= :'lapse' order by enforced,add;
```

## Complain
Create a complaining text and find its destinator (NOCs/domain owners/...)
https://metacpan.org/pod/Net::Whois::Raw , https://metacpan.org/pod/Net::Whois::IANA
https://metacpan.org/pod/Email::ARF

## IPv6
