-- -*- mode: sql; sql-product: postgres; -*-
\set attack_penalty_max_age 2 month
\set attack_penalty_base 100000
\set attack_penalty_exponent 1.9
\set attack_penalty_unit 7200
\set attack_penalty_max 1728000

-- DELETE any address associated to an elapsed penalty
DELETE from attacker_penalty where enforced + cast(penalty||' second' as interval)<now();

-- INSERT any address not already present and which must be included
INSERT INTO attacker_penalty (add, penalty) select a.add, case when ( :attack_penalty_base + ( count(*) ^ :attack_penalty_exponent * :attack_penalty_unit) ) > :attack_penalty_max then :attack_penalty_max else ( :attack_penalty_base + ( count(*) ^ :attack_penalty_exponent * :attack_penalty_unit)::integer ) end as penalty from attacker_ip a left join attacker_penalty p on p.add=a.add where p.add is NULL and a.add in (select distinct add from attacker_ip p where (select now()-max(detected) from attacker_ip t where t.add=p.add)<:'attack_penalty_max_age') group by a.add having count(*)>1;

\o :SCRIPT_FILE
SELECT 'ipset -exist add '||:'IPSET_NAME'||' '||add||' timeout '||penalty from attacker_penalty where now()-enforced < '1 minute';
